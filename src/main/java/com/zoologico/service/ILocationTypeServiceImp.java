package com.zoologico.service;

import com.zoologico.domain.LocationType;
import com.zoologico.repository.LocationTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ILocationTypeServiceImp implements ILocationTypeService {

    @Autowired
    public LocationTypeRepository locationTypeRepository;

    @Override
    public LocationType create(LocationType locationType) {
        return locationTypeRepository.save(locationType);
    }

    @Override
    public Iterable<LocationType> read() {
        return locationTypeRepository.findAll();
    }
}
