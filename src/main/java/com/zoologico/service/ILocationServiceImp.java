package com.zoologico.service;

import com.zoologico.domain.Location;
import com.zoologico.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ILocationServiceImp implements ILocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Location create(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public Iterable<Location> read() {
        return locationRepository.findAll();
    }
}
