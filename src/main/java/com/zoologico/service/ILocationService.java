package com.zoologico.service;

import com.zoologico.domain.Location;

public interface ILocationService {

    public Location create(Location location);

    public Iterable<Location> read();
}
