package com.zoologico.service;

import com.zoologico.domain.LocationType;

public interface ILocationTypeService {

    public LocationType create(LocationType locationType);

    public Iterable<LocationType> read();

}
