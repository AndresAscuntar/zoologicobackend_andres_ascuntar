package com.zoologico.domain.enumeration;

public enum Gender {
    MALE, FEMALE
}
