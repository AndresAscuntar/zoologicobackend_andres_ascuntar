package com.zoologico.domain;

import com.zoologico.domain.enumeration.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Animal {
    @Id
    private String code;

    private String name;
    private String race;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToOne
    private Location location;


}
