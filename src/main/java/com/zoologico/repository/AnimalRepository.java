package com.zoologico.repository;

import com.zoologico.domain.Animal;
import com.zoologico.repository.dto.AnimalNameGender;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AnimalRepository extends CrudRepository<Animal, String> {
    @Query(value = "select count(animal)from Animal animal")
    Integer quantityAnimals();

    @Query(value = "select animal.name as name, animal.gender as gerder from Animal animal")
    Iterable<AnimalNameGender> findAnimalNameGender();

    @Query(value = "select count(animal.gender) from Animal animal")
    Integer quantityGender();
}
