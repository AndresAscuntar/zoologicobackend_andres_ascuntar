package com.zoologico.repository;

import com.zoologico.domain.LocationType;
import org.springframework.data.repository.CrudRepository;

public interface LocationTypeRepository extends CrudRepository<LocationType, Integer> {
}
