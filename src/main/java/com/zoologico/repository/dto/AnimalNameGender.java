package com.zoologico.repository.dto;

public interface AnimalNameGender {
    public String getName();
    public String getGender();
}
